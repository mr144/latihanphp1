<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body style="background-image: url('https://images.unsplash.com/photo-1485841890310-6a055c88698a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80');">
	<nav class="navbar navbar-dark bg-dark">
	  	<a class="navbar-brand" href="#">SEATRANS</a>
	</nav>
	<div class="container mt-5">
		<div class="card border-success">
  			<div class="card-body">
				<h3>Selamat Datang di ajang pencarian Boyband/Girlband Seatrans 2020</h3>
				<p>Masukkan data diri anda</p>
				<hr>
  				<form>
			  		<div class="form-group row">
			    		<label class="col-sm-2 col-form-label">Nama</label>
			    		<div class="col-sm-6">
			      			<input type="text" class="form-control">
			    		</div>
			  		</div>
			  		<div class="form-group row">
			    		<label class="col-sm-2 col-form-label">Usia</label>
			    		<div class="col-sm-6">
			      			<select class="form-control">
			      				<option value="1">dibawah 20 tahun</option>
			      				<option value="2">diantara 20 sampai 22 tahun</option>
			      				<option value="3">diatas 22 tahun</option>
			      			</select>
			    		</div>
			  		</div>
			  		<div class="form-group row">
			    		<label class="col-sm-2 col-form-label">Asal Kota</label>
			    		<div class="col-sm-6">
			      			<input type="text" class="form-control">
			    		</div>
			  		</div>
			  		<fieldset class="form-group">
					    <div class="row">
					      	<legend class="col-form-label col-sm-2 pt-0">Boyband/Girlband</legend>
				      		<div class="col-sm-10">
				        		<div class="form-check">
				          			<input class="form-check-input" type="radio" id="gridRadios1" value="1" checked>
				          			<label class="form-check-label" for="gridRadios1">
				            			Boyband
				          			</label>
				        		</div>
				       			 <div class="form-check">
				          			<input class="form-check-input" type="radio" id="gridRadios2" value="2">
				          			<label class="form-check-label" for="gridRadios2">
				            			Girlband
				          			</label>
				        		</div>
				        	</div>
					  	</div>
					</fieldset>
					<div class="form-group row">
			    		<label class="col-sm-2 col-form-label">Nama Boyband/Girlband</label>
			    		<div class="col-sm-10">
			      			<input type="text" class="form-control">
			    		</div>
			  		</div>
			  		<div class="form-group row">
			    		<label class="col-sm-2 col-form-label">Tagline</label>
			    		<div class="col-sm-10">
			      			<textarea class="form-control"></textarea>
			    		</div>
			  		</div>
			  		<div class="form-group row">
    					<div class="col-sm-10 offset-sm-2">
      						<button type="submit" class="btn btn-primary">Simpan</button>
    					</div>
  					</div>
			  	</form>
  			</div>
  		</div>		
	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>