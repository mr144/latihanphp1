<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body style="background-image: url('https://images.unsplash.com/photo-1490312278390-ab64016e0aa9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80');">
	<nav class="navbar navbar-dark bg-dark">
	  	<a class="navbar-brand" href="#">SEATRANS</a>
	</nav>
	<div class="container mt-5">
		<div class="card border-info">
  			<div class="card-body">
  				<h4>Data Diri Anda
  					<a href="index.php" class="btn btn-secondary float-right">Form pendaftaran</a>
  				</h4>
  				<!-- JIKA PILIH BOYBAND -->
  				<img src="https://kprofiles.com/wp-content/uploads/2016/09/Jin-2-900x600.jpg" width="150px" height="200px" style="object-fit: cover;border: 1px solid black;">
  				<!-- JIKA PILIH GIRLBAND -->
  				<img src="https://fashionseoul.com/wp-content/uploads/2019/04/20190417_DAMIANI_01.jpg" width="150px" height="200px" style="object-fit: cover;border: 1px solid black;">
  				<br><br>
				<table class="table table-striped">
					<tbody>
						<tr>
							<th width="25%">Nama</th>
							<th width="5%">:</th>
							<td></td>
						</tr>
						<tr>
							<th>Usia</th>
							<th>:</th>
							<td></td>
						</tr>
						<tr>
							<th>Asal Kota</th>
							<th>:</th>
							<td></td>
						</tr>
						<tr>
							<th>Boyband/Girlband</th>
							<th>:</th>
							<td></td>
						</tr>
						<tr>
							<th>Nama Boyband/Girlband</th>
							<th>:</th>
							<td></td>
						</tr>
						<tr>
							<th>Tagline</th>
							<th>:</th>
							<td></td>
						</tr>
					</tbody>
				</table>
  			</div>
  		</div>		
	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>